# -*- coding: utf-8 -*-
import sh
import pyudev
import uuid

from .selector import *

"""Mass Storage Switcher (MASS)

Mass Storage Switcher (MASS) is a board developed by the Pleiades to allow to
connect to the cloud devices which only support exchange their information via
pendrive.

MASS works controlling a USB selector allowing to connect a real pendrive or to
the device or the beaglebone.

MASS DIAGRAM
+------------------------------+
|                              |
│                    +---+     |   +------+
|                    |   |=========|DEVICE|
|                    | S |     |   +------+
|                    | E |     |
|                    | L |     |   +--------+
|        GPIO.26 ----+ E |=========|PENDRIVE|
|                    | C |     |   +--------+
|                    | T |     |
|                    | O |     |   +----------+
|                    | R |=========|BEAGLEBONE|
|                    +---+     |   +----------+
|                              |
+------------------------------+

OBSERVATIONS:
. GPIO.26 == P8_14 (header P8 pin 14)

TODO:
  * Include LED control (from PCA9500)
  * Include EEPROM (from PCA9500)
  * Include USB2660
  * Monitor SDCARD activity from USB2660 LED activity
"""

class MassInterface:
    def mount(self, device_node, name):
        pass

    def umount(self, device_node):
        pass

class MassAdapter(MassInterface):
    def mount(self, device_node, abs_path):
        sh.pmount(device_node, abs_path)

    def umount(self, device_node):
        sh.pumount(device_node)

class Mass(object):
    def __init__(self, adapter=None):
        self._device_nodes = dict()
        self._cbs_on_mounted = set()
        self._selector = Selector()
        # Factory decision:
        # 1st. consider external adapter
        # 2nd. if None, consider internal adapter
        self._adapter = adapter
        if None == self._adapter:
            self._adapter = MassAdapter()
        # Create monitor device
        self._context = pyudev.Context()
        self._monitor = pyudev.Monitor.from_netlink(self._context)
        self._monitor.filter_by('block')
        self._observer = pyudev.MonitorObserver(self._monitor,
                callback=self._check_udev_block, name="monitor-block")
        self._observer.start()

    def _call_on_mounted(self, abs_path):
        for callback in self._cbs_on_mounted:
            callback(abs_path)

    def _mount_partition(self, device_node):
        abs_path = "/media/" + str(uuid.uuid4())
        self._device_nodes[device_node] = abs_path
        self._adapter.mount(device_node, abs_path)
        self._call_on_mounted(abs_path)

    def _check_udev_block(self, device):
        if 'partition' == device.device_type:
            if 'add' == device.action:
                self._mount_partition(device.device_node)

    def _umount_partition(self, device_node):
        self._adapter.umount(device_node)
        del self._device_nodes[device_node]

    def _umount_all_partitions(self):
        for device_node, folder in self._device_nodes.copy().items():
            self._umount_partition(device_node)

    def sync(self):
        self._selector.to_bbb()

    def release(self):
        self._umount_all_partitions()
        self._selector.to_device()

    def on_mounted(self, callback):
        self._cbs_on_mounted.add(callback)

