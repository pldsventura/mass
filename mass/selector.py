# -*- coding: utf-8 -*-

import Adafruit_BBIO.GPIO as gpio

class Selector(object):
    """Mass class used to control the mass board.

    To use:
    >>> import mass
    >>> selector = mass.Selector()
    >>> selector.to_bbb()
    >>> selector.to_device()
    """

    def __init__(self, pin_name='P8_14'):
        self.__pin_name = pin_name
        gpio.setup(self.__pin_name, gpio.OUT)

    def to_bbb(self):
        """Mount pendrive in the beaglebone.

        Switch the pendrive USB to the beaglebone and mount it in the Linux to
        sync the files to the cloud.
        """
        gpio.output(self.__pin_name, gpio.HIGH)

    def to_device(self):
        """Unmount pendrive from beaglebone.

        Unmount the pedrive from beaglebone and switch back the pendrive USB to
        the device.
        """
        gpio.output(self.__pin_name, gpio.LOW)

