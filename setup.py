import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mass",
    version="0.0.1",
    author="Leandro dos Santos",
    author_email="lsantosp@gmail.com",
    description="Mass Storage Switcher",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/pldsventura/mass/src",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)

